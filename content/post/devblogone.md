---
title: "The Finish Line"
summary: "For our first DevBlog, we look back at how Croissants came together at the very end."
draft: false
date: "2020-05-06"
categories: [
    "DevBlog",
]
image: "finishline.jpg"
# https://pxhere.com/en/photo/1551255
---

I'm not sure when I first heard it mentioned (probably on a podcast) but a video game is not a video game until the last 5% of its development time. It certainly doesn't look, sound, nor play like one. 
No matter the state of your assets, how they are arranged, or the todo items you have checked off, it's a pile of stuff. Then, it undergoes a transformation as you squash the whole thing together and throw it out the front door to face the world at large.

This was certainly how Croissants felt in October 2019, when Hugh and I realised that we needed to actually finish the game and release it into the world. We still had tons of ideas for it, things we thought would be a good fit, pipe dreams of every sort, and we took a knife to them in order to have something we may actually put together and finish before 2030.

It was honestly liberating to have this pressure on, at least for me. We had to justify anything not already working or cull it, which gave us a solid path for what we needed to do to finish. To belabour this metaphor further, each step we took now made our project more of a video game. Before this point even solid work was still an experiment, an idea, a possibility that was thrown into a useful pile. 
Now, a real game started to appear. While the BGM had existed for most of the project, adding sound effects made actions feel actually interactive. Looking at elements and saying "Yep, that's it" was a massive relief as while the internal voice was still yelling "nooo nooo you can change it, improve it, make it better" you could shut it away and know you were moving closer to completion.

Known bugs and things we had taken as expected for the majority of the development lifespan were now brought forward and actually fixed rather than lived with. This was especially true of the controls, and I feel it has to be a common element in design and development of all kinds that if you are actually making the thing you will happily justify your busted control/interaction scheme for as long as you can. Showing it to others will remind you that most people DO mind if they have to interact with your game via a scorpion-shaped controller that stings them every time they press "Y".

This process of putting everything together also pushed some issues to the fore - mostly remnants from loose ideas we had come up with earlier in development, or compromises that we had made in the expectation that some grand "real version" would come along later. 
At this point, we had the final version whether we wanted it or not. The other side effect of this was finding out that some elements of the games scripting were not fit for purpose at all and I'm pretty sure Hugh re-wrote some elements of the game three times over as issues arose that laid waste to previous work in such a way that a re-write was much easier than "fixing it". 
This is the grand "Chickens coming home to roost" moment where you get to see the shortcomings in what you have made and need to make peace with them. Something else I have heard from other developers (not just game developers) is that they are acutely aware of the shortcomings in what they release - they know it well. 
You can't work on it forever, and I say this bitterly as someone who has untold numbers of unfinished projects laid low by ruinous perfectionism.

It's not all bad though - as these small illuminating changes add up and you continue to bash away at it, you get to see the final game emerge through the many different builds you throw together. It's honestly exciting to see the iteration, even if it did mean annoying our valiant beta-testers. Often we would have to provide sheepish admissions that the version they had very kindly tested was out of date and a good 80% of their feedback had already been acted on and would you mind testing THIS new version instead? Please? Pretty Please? Hey why are you walking away in a huff come back nooooo

But through this, the game comes together. At some point even releasing it becomes a foregone conclusion, of a sort. It helped in our case that we needed to get the game out of our heads before Hugh left for an extended trip, and personally once the big "PUBLISH" button was pushed I felt nothing but relief. 
We released a game! An Actual Video Game! 

And now we finally had time and space to entertain the ideas for the next one.
