---
title: "Resident... Evil... Two..."
summary: "Elliot takes an overdue look at the Resident Evil 2 Remake and what he likes about it."
draft: true
date: "2020-05-08"
categories: [
    "DevBlog",
]
image: "resi2top.png"
---

The Resident Evil 2 remake was on sale for £20 on PSN recently and I decided to take a punt on it. I have already watched one and a half playthroughs on it thanks to the wonders of internet streaming video but I fancied a calm jaunt through zombieland to take my mind off things.

Setting aside the current ongoing Remake/Reimagining/Remaster discussion that has been tying podcasters in knots recently, Resident Evil 2 is a very good game. I have enjoyed my time with it so far, especially as someone who wanted to like the original versions of these games on the Saturn/Playstation/Dreamcast but was total garbage at them. 

I wanted to discuss some elements of the game here, both those I like and don't like as much:

&nbsp;

### + Map
The Map in Resident Evil 2 is great, and many other people have already waxed lyrical about it. It marks if you have "completed" a room and also puts little icons on the map for pickups that you have passed by - all you have to do is swing the camera over them and it gets added. This is a massive help in a game with limited resources and limited inventory space for multiple reasons - it lets you know where you should focus your time and efforts, most particulaly. 

It also loads in a split-second, which is a godsend for maintaining tension and also for letting you quickly "dip into" the map whenever you need without having to perform any internal calculus of "how long is this going to take me out of the game".

&nbsp;

### + Game / Objective Flow
The game always provides you with a "top-line" objective to complete, which is honestly more of a narrative imperative and I never referred to. On a more granular and immediate level, Resident Evil 2 is always providing you with 1-2 immediate things to accomplish. Explore that new area. Use this key on that door. Pick up that item you missed. Investigate that statue we marked on your map. Run away from this massive looming threat we provide in the second quartile of the game just as you are settling into a comfortable rythym and who leaves an overwhelming impression on you.

This is a massive improvement over the original version of the game where my lasting impression is one of being constantly stuck and unable to determine where I need to go or what cryptic macguffin I need to pick up.

&nbsp;

### + The SLS 60
The SLS 60 is the starter gun you have when you play as Claire. It is a 5-shot revolver with a long reload time (quickly fixed with an upgrade part) and a wide bullet spread, so you have to really pick your shots with it. Its great at providing tension in the early game, but is thankfully replaced as your standard handgun in short order. 

In the last third of the game you can find another upgrade part that allows it to take "high-power" ammunition, turning it into that standard end-game weapon of this game series, the magnum. 

This is admittedly a small thing but I personally really like it- the gun retains all its remaining drawbacks (long aiming time, long time between shots) but it is now given a new lease on life. It also helps prevent weapon sprawl which can arguably happen in games of this nature - instead of giving you another limited use weapon it boosts an existing one back into your roster. 

&nbsp;

### - Player Character Barks
Do you really need to yell "Bastard!" at the zombies? They can't hear you, that's pretty much their whole deal. 

I understand providing the player characters with some personality and in-world reactions - piloting a mute around a creepy situation apart from when they take damage is also bad but don't be mean to the living dead. 

&nbsp;

### - No Red9
The Red9 handgun in Resident Evil 4 is the pinnacle of video game handguns and whenever it is not present I am compelled to complain. 

&nbsp;

### And Finally 
As a final note, I have refrained from calling the game "Resi 2" in this post because it feels extremely 90's nowadays. No-one wants to go back to the 90's.