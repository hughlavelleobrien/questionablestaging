---
title: "Croissants"
date: 2020-01-16T21:24:49Z
draft: false
image: "croissantScreenshots/conveyer.png"
type: "page"
summary: "A system exploring both Croissants and Life"
---

There comes a time in everyone's life when they question their place in the world.

Am I just a cog in the machine?

...

...

...

Also where do different pastry varieties come from?

Both of these questions, one existential and the other pastry based, are explored in what we present to you here. 

**Available now:**

{{< itchioLink game="croissants" >}}

--------------------------
# Trailer

{{< croissantTrailer >}}

--------------------------

# The System

## A pastry assembly robot management simulator.

The system is a balancing act between three main goals:

## Fulfilling orders

{{< screenshot path="croissantScreenshots/conveyer.png" >}}
{{< screenshot path="croissantScreenshots/convert.png" >}}

## Maintaining your body with sweet pastries

{{< screenshot path="croissantScreenshots/eat.png" >}}

## Contemplating the futility of existence

{{< screenshot path="croissantScreenshots/existence.png" >}}

## Featuring

- 8 Story Levels
- Endless mode
- Achievements
- Controller support
- Love
- Monochrome mode

{{< screenshot path="croissantScreenshots/monochrome.png" >}}

- Linux support

{{< screenshot path="croissantScreenshots/tux.png" >}}

