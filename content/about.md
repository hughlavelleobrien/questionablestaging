---
title: "About Questionable Systems"
date: 2020-01-16T20:37:04Z
draft: false
image: "Q_icon.png"
type: "page"
summary: "Who are QS?"
---
Questionable Systems is:

* **[Hugh O'Brien](https://twitter.com/fuzzymooples)**
  * Once Ireland, Now England
  * +1 to hit
  * &nbsp;  
* **[Elliot Page](https://twitter.com/elliotpage)**
  * Once England, Now Scotland
  * +1 to weep
  * &nbsp;  

# We hope to craft experiences that will make you say "*I'm not sure about this, but I'll allow it.*"  
Questionable Systems Ltd.  
Established 2016.  
Incorporated 2020.  
Forever 24 Years Old.  