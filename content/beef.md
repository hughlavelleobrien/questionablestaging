---
title: "Historical Beef"
date: 2020-01-16T21:00:04Z
draft: false
image: "Hist_beef_top_wide.png"
type: "page"
summary: "An art exhibition curation simulator"
---

In a beautiful future where the pursuit of art is unending, you finally have the freedom to construct an exhibition dedicated to something that has been sadly ignored until now: Meat in Video Games.

**Available now:**

{{< itchioLink game="historical-beef" >}}

--------------------------
# The System

## An Art Exhibition Curation Simulator paying homage to Video Game Ingestibles.

Created as part of Wizard Jam 4, Historial Beef is the first work created by the team that would later be called Questionable Systems.

## Featuring

- 3 Campaign Levels
- Freestyle mode
- Achievements
- Writing
- An Easter Egg
- A Highly Questionable Judging Algorithm